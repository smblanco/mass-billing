<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EstadoCobroServicio extends Model
{
    public $table = "estados_cobro_servicio";

    const ACTIVO = 1;
    const PAGO_PENDIENTE = 2;
    const NO_ACTIVO = 3;
    const NO_PAGADO = 4;
    const ERROR_EN_PAGO = 5;
    const PENDIENTE = 6;
}
