<?php

namespace App\Http\Controllers\Backend\ComentariosClientes;

use Illuminate\Http\Request;
use App\Http\Controllers\Backend\ABM_Core;

use App\Library\ResponseEstructure;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;

use Response;
use App\ComentarioCliente;

class ComentariosClientesController extends ABM_Core
{
    public function __construct()
    {
        $this->link_controlador = url('/backend/comentarios_clientes').'/';
        $this->entity ="Comentario";
        $this->title_page = "Comentarios Clientes";

        $this->columns = array(
            ["name"=>"Comentario","reference"=>"comentarios_clientes.texto"],
            ["name"=>"Calificacion","reference"=>"comentarios_clientes.calificacion"],
            ["name"=>"Activo","reference"=>"comentarios_clientes.activo"],
            ["name"=>"Imagen","reference"=>"comentarios_clientes.imagen"],
            ["name"=>"Acciones","reference"=>null]
        );

        $this->is_ajax = true;
    }

    public function index(Request $request)
    {
        $this->validar_administrador();
        
        $this->share_parameters();
        return View("backend.comentarios_clientes.browse");
    }

    public function get_listado_dt(Request $request)
    {
        $this->validar_administrador();

        $consulta_orm_principal = DB::table("comentarios_clientes")
        ->select(
            "comentarios_clientes.*"
        );

    	$totalData = $consulta_orm_principal->count();
        
        $totalFiltered = $totalData;
        
        $search = $request->input("search");
        $start = $request->input('start');
        $length = $request->input('length');
        $order = $request->input('order');

        $resultado = array();

        if(!empty($search['value'])) 
        {
            $consulta_orm_principal = $consulta_orm_principal_principal
            ->where(function($query) use($search){
                $query->where("comentarios_clientes.texto","like","%".$search['value']."%")
                ->orWhere("comentarios_clientes.calificacion","like","%".$search['value']."%");
            });

            $resultado= $consulta_orm_principal->take($length)->skip($start)->orderBy("id","desc")->get();

            $totalFiltered=$consulta_orm_principal->count();
        }
        
        $consulta_orm_principal = $consulta_orm_principal->take($length)->skip($start);
        $totalFiltered=$consulta_orm_principal->count();

        $columna_a_ordenar = (int)$order[0]["column"];

        if(isset($this->columns[$columna_a_ordenar]) && $this->columns[$columna_a_ordenar]["reference"] != null){
          $resultado = $consulta_orm_principal->orderBy($this->columns[$columna_a_ordenar]["reference"],$order[0]["dir"])->get();
        }
        else{
          $resultado = $consulta_orm_principal->orderBy("id","desc")->get();
        }
    	
        $data= array();
        
        foreach($resultado as $result_row)
        {
            $row_of_data = array();

            $row_of_data[]=strip_tags($result_row->texto);
            $row_of_data[]=strip_tags($result_row->calificacion);

            if($result_row->activo)
            {
                $row_of_data[]='<span class="badge text-white" style="background-color: #28a745;">SI</span>';
            }
            else
            {
                $row_of_data[]='<span class="badge text-white" style="background-color: #dc3545;">NO</span>';
            }

            $row_of_data[]="<img src='".asset('storage/imagenes/comentarios_clientes/'.strip_tags($result_row->imagen))."' style='width: 50px;' />";

            

            $buttons_actions = "<div class='form-button-action'>";

            if($this->edit_active)
            {
                if($this->is_ajax)
                {
                    $buttons_actions .= 
                    "<button onclick='abrir_modal_editar(".$result_row->id.")' type='button' data-toggle='tooltip' title='' class='btn-sm ".$this->config_buttons["edit"]["class"]."' data-original-title='".$this->config_buttons["edit"]["title"]."'>
                        <i class='".$this->config_buttons["edit"]["icon"]."'></i>
                    </button>";
                }
                else{
                    $buttons_actions .= 
                    "<a href='".$this->link_controlador."editar/".$result_row->id."' data-toggle='tooltip' title='' class='btn-sm ".$this->config_buttons["edit"]["class"]."' data-original-title='".$this->config_buttons["edit"]["title"]."'>
                        <i class='".$this->config_buttons["edit"]["icon"]."'></i>
                    </a>";
                }
            }

            if($this->delete_active)
            {
                $buttons_actions.=
                "<button onclick='eliminar(".$result_row->id.")' type='button' data-toggle='tooltip' title='' class='btn-sm ".$this->config_buttons["delete"]["class"]."' data-original-title='".$this->config_buttons["delete"]["title"]."'>
                    <i class='".$this->config_buttons["delete"]["icon"]."'></i>
                </button>";
            }

            $buttons_actions.="</div>";

            
            $row_of_data[]=$buttons_actions;

            $data[]=$row_of_data;
        }

		$json_data = array(
            "draw"            => intval($request->input('draw')), 
            "recordsTotal"    => intval($totalData), 
            "recordsFiltered" => intval($totalFiltered), 
            "data"            => $data
        );

    	return response()->json($json_data);
    }

    public function get(Request $request)
    {
        $this->validar_administrador();

        $response_estructure = new ResponseEstructure();
        $response_estructure->set_response(false);

        $id = $request->input("id");

        $row_obj = ComentarioCliente::find($id);
        
        if($row_obj)
        {
            $response_estructure->set_response(true);
            $response_estructure->set_data($row_obj);
        }
        else
        {
            $response_estructure->set_response(false);
            $response_estructure->add_message_error($this->text_no_search);
        }
        

        return response()->json($response_estructure->get_response_array());
    }

    public function store(Request $request)
    {
        $this->validar_administrador();

        $response_estructure = new ResponseEstructure();
        $response_estructure->set_response(false);

        $texto = $request->input("texto");
        $calificacion = $request->input("calificacion");
        $activo = $request->input("activo");
        $imagen = $request->input("imagen");

        $input= [
            "texto" => $texto,
            "calificacion" => $calificacion,
            "activo" => $activo,
            "imagen" => $imagen
        ];

        $rules = [
            "texto" =>" required",
            "calificacion"  =>"required|numeric|min:1|max:5",
            "activo"    =>" required",
            "imagen"    =>" required",
        ];

        $validator = Validator::make($input, $rules);

        if ($validator->fails()) {
            $response_estructure->set_response(false);

            $errors = $validator->errors();

            foreach ($errors->all() as $error) {
                $response_estructure->add_message_error($error);
            }
        }
        else
        {
            $row_obj = new ComentarioCliente();
            $row_obj->imagen = $imagen;
            $row_obj->texto = $texto;
            $row_obj->calificacion = $calificacion;
            $row_obj->activo = $activo;
            $row_obj->save();

            $response_estructure->set_response(true);
        }

        return response()->json($response_estructure->get_response_array());
    }

    public function update(Request $request)
    {
        $this->validar_administrador();

        $response_estructure = new ResponseEstructure();
        $response_estructure->set_response(false);

        $id = $request->input("id");

        $row_obj = ComentarioCliente::find($id);
        
        if($row_obj)
        {
            $texto = $request->input("texto");
            $calificacion = $request->input("calificacion");
            $activo = $request->input("activo");
            $imagen = $request->input("imagen");

            $input= [
                "texto" => $texto,
                "calificacion" => $calificacion,
                "activo" => $activo,
                "imagen" => $imagen
            ];

            $rules = [
                "texto" =>" required",
                "calificacion"  =>"required|numeric|min:1|max:5",
                "activo"    =>" required",
                "imagen"    =>" required",
            ];


            $validator = Validator::make($input, $rules);

            if ($validator->fails()) {
                $response_estructure->set_response(false);

                $errors = $validator->errors();

                foreach ($errors->all() as $error) {
                    $response_estructure->add_message_error($error);
                }
            }
            else
            {
                $row_obj->imagen = $imagen;
                $row_obj->texto = $texto;
                $row_obj->calificacion = $calificacion;
                $row_obj->activo = $activo;
                $row_obj->save();

                $response_estructure->set_response(true);
            }
        }
        else
        {
            $response_estructure->set_response(false);
            $response_estructure->add_message_error("Registro no encontrado");
        }
        
        return response()->json($response_estructure->get_response_array());
    }

    public function delete(Request $request)
    {
        $this->validar_administrador();
        
        $response_estructure = new ResponseEstructure();
        $response_estructure->set_response(false);

        $id = $request->input("id");

        $row_obj = ComentarioCliente::find($id);
        
        if($row_obj)
        {
            $row_obj->save();
            $response_estructure->set_response(true);
        }
        else
        {
            $response_estructure->set_response(false);
            $response_estructure->add_message_error($this->text_no_search);
        }


        return response()->json($response_estructure->get_response_array());
    }
}
