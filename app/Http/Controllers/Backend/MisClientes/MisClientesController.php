<?php
namespace App\Http\Controllers\Backend\MisClientes;

use Illuminate\Http\Request;
use App\Http\Controllers\Backend\ABM_Core;

use App\Library\ResponseEstructure;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;

use App\CondicionIva;
use App\MisClientes;

use App\Library\WebserviceAfip\WebserviceAfip;

class MisClientesController extends ABM_Core
{
    public function __construct()
    {
        $this->link_controlador = url('/backend/mis_clientes').'/';
        $this->entity ="Cliente";
        $this->title_page = "Mis Clientes";

        $this->columns = array(
            ["name"=>"Condicion Iva","reference"=>"id_condicion_iva"],
            ["name"=>"Tipo documento","reference"=>"tipos_documentos.descripcion"],
            ["name"=>"Documento","reference"=>"documento"],
            ["name"=>"Razon Social","reference"=>"nombre_razon_social"],
            ["name"=>"Correo","reference"=>"correo"],
            ["name"=>"Teléfono","reference"=>"telefono"],
            ["name"=>"Acciones","reference"=>null]
        );

        $this->is_ajax = true;
    }
    
    public function index(Request $request)
    {
        $this->share_parameters();

        $condiciones_iva = CondicionIva::orderBy("descripcion")->get();

        return View("backend.mis_clientes.browse")
        ->with("condiciones_iva",$condiciones_iva);
    }

    public function get_listado_dt(Request $request)
    {
        $id_configuracion_afip = $request->session()->get("id_configuracion_afip");

        $consulta_orm_principal = DB::table("mis_clientes")
        ->leftJoin("condiciones_iva","condiciones_iva.id","=","mis_clientes.id_condicion_iva")
        ->leftJoin("tipos_documentos","tipos_documentos.id","=","mis_clientes.id_tipo_documento")
        ->select(
            "mis_clientes.*",
            "condiciones_iva.descripcion as condiciones_iva_descripcion",
            "tipos_documentos.descripcion as tipos_documentos_descripcion"
        )
        ->where("eliminado",0)
        ->where("id_configuracion_afip",$id_configuracion_afip);

        $totalData = $consulta_orm_principal->count();

        $totalFiltered = $totalData;

        $search = $request->input("search");
        $start = $request->input('start');
        $length = $request->input('length');
        $order = $request->input('order');

        $resultado = array();

        if(!empty($search['value']))
        {
            $consulta_orm_principal = $consulta_orm_principal
            ->where(function($query) use($search){
                $query->where("mis_clientes.nombre_razon_social","like","%".$search['value']."%")
                ->orWhere("mis_clientes.cuit","like","%".$search['value']."%")
                ->orWhere("mis_clientes.correo","like","%".$search['value']."%")
                ->orWhere("mis_clientes.telefono","like","%".$search['value']."%")
                ->orWhere("condiciones_iva.descripcion","like","%".$search['value']."%"); 
            });
        }

        $consulta_orm_principal = $consulta_orm_principal->take($length)->skip($start);
        $totalFiltered=$consulta_orm_principal->count();

        $columna_a_ordenar = (int)$order[0]["column"];

        if(isset($this->columns[$columna_a_ordenar]) && $this->columns[$columna_a_ordenar]["reference"] != null){
          $resultado = $consulta_orm_principal->orderBy($this->columns[$columna_a_ordenar]["reference"],$order[0]["dir"])->get();
        }
        else{
          $resultado = $consulta_orm_principal->orderBy("id","desc")->get();
        }
    	
        $data= array();
        
        foreach($resultado as $result_row)
        {
            $row_of_data = array();

            $row_of_data[]  =   strip_tags($result_row->condiciones_iva_descripcion);
            $row_of_data[]  =   strip_tags($result_row->tipos_documentos_descripcion);
            $row_of_data[]  =   strip_tags($result_row->documento);
            $row_of_data[]  =   strip_tags($result_row->nombre_razon_social);
            $row_of_data[]  =   strip_tags($result_row->correo);
            $row_of_data[]  =   strip_tags($result_row->telefono);
            
            $buttons_actions = "<div class='form-button-action'>";

            if($this->edit_active)
            {
                if($this->is_ajax)
                {
                    $buttons_actions .= 
                    "<button onclick='abrir_modal_editar(".$result_row->id.")' type='button' data-toggle='tooltip' title='' class='btn-sm ".$this->config_buttons["edit"]["class"]."' data-original-title='".$this->config_buttons["edit"]["title"]."'>
                        <i class='".$this->config_buttons["edit"]["icon"]."'></i>
                    </button>";
                }
                else{
                    $buttons_actions .= 
                    "<a href='".$this->link_controlador."editar/".$result_row->id."' data-toggle='tooltip' title='' class='btn-sm ".$this->config_buttons["edit"]["class"]."' data-original-title='".$this->config_buttons["edit"]["title"]."'>
                        <i class='".$this->config_buttons["edit"]["icon"]."'></i>
                    </a>";
                }
            }

            if($this->delete_active)
            {
                $buttons_actions.=
                "<button onclick='eliminar(".$result_row->id.")' type='button' data-toggle='tooltip' title='' class='btn-sm ".$this->config_buttons["delete"]["class"]."' data-original-title='".$this->config_buttons["delete"]["title"]."'>
                    <i class='".$this->config_buttons["delete"]["icon"]."'></i>
                </button>";
            }

            $buttons_actions.="</div>";

            
            $row_of_data[]=$buttons_actions;

            $data[]=$row_of_data;
        }

		$json_data = array(
            "draw"            => intval($request->input('draw')), 
            "recordsTotal"    => intval($totalData), 
            "recordsFiltered" => intval($totalFiltered), 
            "data"            => $data
        );

    	return response()->json($json_data);
    }

    public function get(Request $request)
    {
        $id_configuracion_afip = $request->session()->get("id_configuracion_afip");

        $response_estructure = new ResponseEstructure();
        $response_estructure->set_response(false);

        $id = $request->input("id");

        $row_obj = MisClientes::where("id",$id)->where("id_configuracion_afip",$id_configuracion_afip)->first();
        
        if($row_obj)
        {
            $this->ws_afip = new WebserviceAfip($id_configuracion_afip);

            $tipos_de_documentos = $this->ws_afip->get_tipos_de_documentos_unicos($row_obj->id_condicion_iva);
            $row_obj->tipos_de_documentos = $tipos_de_documentos;

            $response_estructure->set_response(true);
            $response_estructure->set_data($row_obj);
        }
        else
        {
            $response_estructure->set_response(false);
            $response_estructure->add_message_error($this->text_no_search);
        }
        

        return response()->json($response_estructure->get_response_array());
    }

    public function store(Request $request)
    {
        $id_configuracion_afip = $request->session()->get("id_configuracion_afip");

        $response_estructure = new ResponseEstructure();
        $response_estructure->set_response(false);

        $id_tipo_documento = trim($request->input("id_tipo_documento"));
        $nombre_razon_social = (trim($request->input("nombre_razon_social")));
        $documento = trim(strtolower($request->input("documento")));
        $correo = trim(strtolower($request->input("correo")));
        $telefono = trim(strtolower($request->input("telefono")));
        $domicilio_comercial = trim(($request->input("domicilio_comercial")));
        $id_condicion_iva = trim(strtolower($request->input("id_condicion_iva")));

        $input= [
            "id_tipo_documento"=>$id_tipo_documento,
            "nombre_razon_social"=>$nombre_razon_social,
            "documento"=>$documento,
            "correo"=>$correo,
            "condicion_iva"=>$id_condicion_iva
        ];

        $rules = [
            "id_tipo_documento"=>"required",
            "nombre_razon_social"=>"required|min:3",
            "documento"=>"required",
            "condicion_iva"=>"required",
        ];

        if(empty($correo) == false)
        {
            $rules["correo"] = "email";
        }

        $validator = Validator::make($input, $rules);

        if ($validator->fails()) {
            $response_estructure->set_response(false);

            $errors = $validator->errors();

            foreach ($errors->all() as $error) {
                $response_estructure->add_message_error($error);
            }
        }
        else
        {
            $response_estructure->set_response(true);

            $mis_clientes_obj = MisClientes::where("documento",$documento)
            ->where("id_configuracion_afip",$id_configuracion_afip)
            ->where("eliminado",0)->first();

            if($mis_clientes_obj)
            {
                $response_estructure->set_response(false);
                $response_estructure->add_message_error("El documento del cliente ya está en uso");
            }

            if($response_estructure->get_response() === TRUE)
            {   
                if(empty($correo)){
                    $correo = null;
                }
                if(empty($telefono)){
                    $telefono = null;
                }
                if(empty($domicilio_comercial)){
                    $domicilio_comercial = null;
                }

                // CUIT
                if($id_tipo_documento == 80)
                {
                    $cuit_limpio = "";

                    for($i=0; $i < strlen($documento);$i++)
                    {
                        if(is_numeric($documento[$i]))
                        {
                            $cuit_limpio .= $documento[$i];
                        }
                    }

                    $documento = $cuit_limpio;
                }

                $row_obj = new MisClientes();

                $row_obj->id_tipo_documento = $id_tipo_documento;
                $row_obj->nombre_razon_social = $nombre_razon_social;
                $row_obj->documento = $documento;
                $row_obj->correo = $correo;
                $row_obj->telefono = $telefono;
                $row_obj->domicilio_comercial = $domicilio_comercial;
                $row_obj->id_condicion_iva = $id_condicion_iva;
                $row_obj->id_configuracion_afip = $id_configuracion_afip;

                $row_obj->save();
            } 
        }

        return response()->json($response_estructure->get_response_array());
    }

    public function update(Request $request)
    {
        $id_configuracion_afip = $request->session()->get("id_configuracion_afip");

        $response_estructure = new ResponseEstructure();
        $response_estructure->set_response(false);

        $id = $request->input("id");

        $row_obj = MisClientes::where("id",$id)
        ->where("id_configuracion_afip",$id_configuracion_afip)
        ->where("eliminado",0)
        ->first();
        
        if($row_obj)
        {
            $id_tipo_documento = trim($request->input("id_tipo_documento"));
            $nombre_razon_social = (trim($request->input("nombre_razon_social")));
            $documento = trim(strtolower($request->input("documento")));
            $correo = trim(strtolower($request->input("correo")));
            $telefono = trim(strtolower($request->input("telefono")));
            $domicilio_comercial = trim(($request->input("domicilio_comercial")));
            $id_condicion_iva = trim(strtolower($request->input("id_condicion_iva")));

            $input= [
                "id_tipo_documento"=>$id_tipo_documento,
                "nombre_razon_social"=>$nombre_razon_social,
                "documento"=>$documento,
                "correo"=>$correo,
                "condicion_iva"=>$id_condicion_iva
            ];
    
            $rules = [
                "id_tipo_documento"=>"required",
                "nombre_razon_social"=>"required|min:3",
                "documento"=>"required",
                "condicion_iva"=>"required",
            ];
    
            if(empty($correo) == false)
            {
                $rules["correo"] = "email";
            }

            $validator = Validator::make($input, $rules);

            if ($validator->fails()) {
                $response_estructure->set_response(false);

                $errors = $validator->errors();

                foreach ($errors->all() as $error) {
                    $response_estructure->add_message_error($error);
                }
            }
            else
            {
                $response_estructure->set_response(true);

                // CUIT
                if($id_tipo_documento == 80)
                {
                    $cuit_limpio = "";

                    for($i=0; $i < strlen($documento);$i++)
                    {
                        if(is_numeric($documento[$i]))
                        {
                            $cuit_limpio .= $documento[$i];
                        }
                    }

                    $documento = $cuit_limpio;
                }

                $mis_clientes_obj = MisClientes::where("documento",$documento)
                ->where("id_configuracion_afip",$id_configuracion_afip)
                ->where("id","<>",$id)
                ->where("eliminado",0)->first();

                if($mis_clientes_obj)
                {
                    $response_estructure->set_response(false);
                    $response_estructure->add_message_error("El documento del cliente ya está en uso");
                }

                if($response_estructure->get_response() === TRUE)
                {   
                    if(empty($correo)){
                        $correo = null;
                    }
                    if(empty($telefono)){
                        $telefono = null;
                    }
                    if(empty($domicilio_comercial)){
                        $domicilio_comercial = null;
                    }

                    $row_obj->id_tipo_documento = $id_tipo_documento;
                    $row_obj->nombre_razon_social = $nombre_razon_social;
                    $row_obj->documento = $documento;
                    $row_obj->correo = $correo;
                    $row_obj->telefono = $telefono;
                    $row_obj->domicilio_comercial = $domicilio_comercial;
                    $row_obj->id_condicion_iva = $id_condicion_iva;

                    $row_obj->save();
                } 
            }
        }
        else
        {
            $response_estructure->set_response(false);
            $response_estructure->add_message_error($this->text_no_search);
        }
        
        return response()->json($response_estructure->get_response_array());
    }

    public function delete(Request $request)
    {
        $id_configuracion_afip = $request->session()->get("id_configuracion_afip");

        $response_estructure = new ResponseEstructure();
        $response_estructure->set_response(false);

        $id = $request->input("id");

        $row_obj = MisClientes::where("id",$id)->where("id_configuracion_afip",$id_configuracion_afip)->first();
        
        if($row_obj)
        {
            $row_obj->eliminado = 1;
            $row_obj->save();
            $response_estructure->set_response(true);
        }
        else
        {
            $response_estructure->set_response(false);
            $response_estructure->add_message_error($this->text_no_search);
        }


        return response()->json($response_estructure->get_response_array());
    }

    public function busquedaClienteNuevoComprobante(Request $request)
    {
        $id_configuracion_afip = $request->session()->get("id");

        $busqueda = $request->input("q");

    	$resultados_busqueda = MisClientes::select("mis_clientes.*")
    	->where("mis_clientes.id_configuracion_afip",$id_configuracion_afip)
    	->where("mis_clientes.eliminado",0)
		->where(function($query) use ($busqueda){
            $query->where("mis_clientes.documento","like","%".$busqueda."%")
            ->orWhere("mis_clientes.nombre_razon_social","like","%".$busqueda."%");
        });

    	$resultados_busqueda =$resultados_busqueda->take(10)
		->get();

    	$respuesta = array();

    	if(count($resultados_busqueda) > 0)
    	{
	    	foreach($resultados_busqueda as $resultado_busqueda)
	    	{
	    		$respuesta[]= array("id"=>$resultado_busqueda->id,"text"=>$resultado_busqueda->documento." - ".$resultado_busqueda->nombre_razon_social );
	    	}
    	}
    	else
    	{
			$respuesta[]= array(""=>"$busqueda","Seleccionar"=>"");
    		$respuesta[]= array("id"=>"Agregar: ".$busqueda,"text"=>"Agregar: ".$busqueda);
    	}

    	return response()->json($respuesta);
    }

}
