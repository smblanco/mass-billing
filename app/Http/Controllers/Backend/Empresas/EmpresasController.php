<?php

namespace App\Http\Controllers\Backend\Empresas;

use Illuminate\Http\Request;
use App\Http\Controllers\Backend\ABM_Core;

use App\Library\ResponseEstructure;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;

use App\ConfiguracionAfip;

use Response;

class EmpresasController extends ABM_Core
{
    public function __construct()
    {
        $this->link_controlador = url('/backend/empresas').'/';
        $this->entity ="Empresa";
        $this->title_page = "Empresas";

        $this->columns = array(
            ["name"=>"Razon social","reference"=>"configuraciones_afip.razon_social"],
            ["name"=>"CUIT","reference"=>"configuraciones_afip.cuit"],
            ["name"=>"Acciones","reference"=>null]
        );

        $this->is_ajax = false;

    }
    
    public function index(Request $request)
    {
        $this->share_parameters();

        return View("backend.empresas.browse");
    }

    public function get_listado_dt(Request $request)
    {
        $consulta_orm_principal = DB::table("configuraciones_afip")
        ->select(
            "configuraciones_afip.*"
        )
        ->where("eliminado",0);

        $totalData = $consulta_orm_principal->count();

        $totalFiltered = $totalData;

        $search = $request->input("search");
        $start = $request->input('start');
        $length = $request->input('length');
        $order = $request->input('order');

        $resultado = array();

        if(!empty($search['value']))
        {
            $consulta_orm_principal = $consulta_orm_principal
            ->where(function($query) use($search){
                $query->where("configuraciones_afip.razon_social","like","%".$search['value']."%")
                ->orWhere("configuraciones_afip.cuit","like","%".$search['value']."%");
            });
        }

        $consulta_orm_principal = $consulta_orm_principal->take($length)->skip($start);
        $totalFiltered=$consulta_orm_principal->count();

        $columna_a_ordenar = (int)$order[0]["column"];

        if(isset($this->columns[$columna_a_ordenar]) && $this->columns[$columna_a_ordenar]["reference"] != null){
          $resultado = $consulta_orm_principal->orderBy($this->columns[$columna_a_ordenar]["reference"],$order[0]["dir"])->get();
        }
        else{
          $resultado = $consulta_orm_principal->orderBy("id","desc")->get();
        }
    	
        $data= array();
        
        foreach($resultado as $result_row)
        {
            $row_of_data = array();

            $row_of_data[]=strip_tags($result_row->razon_social);
            $row_of_data[]=strip_tags($result_row->cuit);
            
            $buttons_actions = "<div class='form-button-action'>";

            if($this->edit_active)
            {
                if($this->is_ajax)
                {
                    $buttons_actions .= 
                    "<button onclick='abrir_modal_editar(".$result_row->id.")' type='button' data-toggle='tooltip' title='' class='btn-sm ".$this->config_buttons["edit"]["class"]."' data-original-title='".$this->config_buttons["edit"]["title"]."'>
                        <i class='".$this->config_buttons["edit"]["icon"]."'></i>
                    </button>";
                }
                else{
                    $buttons_actions .= 
                    "<a href='".$this->link_controlador."editar/".$result_row->id."' data-toggle='tooltip' title='' class='btn-sm ".$this->config_buttons["edit"]["class"]."' data-original-title='".$this->config_buttons["edit"]["title"]."'>
                        <i class='".$this->config_buttons["edit"]["icon"]."'></i>
                    </a>";
                }
            }

            if($this->delete_active)
            {
                $buttons_actions.=
                "<button onclick='eliminar(".$result_row->id.")' type='button' data-toggle='tooltip' title='' class='btn-sm ".$this->config_buttons["delete"]["class"]."' data-original-title='".$this->config_buttons["delete"]["title"]."'>
                    <i class='".$this->config_buttons["delete"]["icon"]."'></i>
                </button>";
            }

            $buttons_actions.="</div>";

            
            $row_of_data[]=$buttons_actions;

            $data[]=$row_of_data;
        }

		$json_data = array(
            "draw"            => intval($request->input('draw')), 
            "recordsTotal"    => intval($totalData), 
            "recordsFiltered" => intval($totalFiltered), 
            "data"            => $data
        );

    	return response()->json($json_data);
    }

    public function nuevo(Request $request)
    {
        $this->title_page = $this->config_buttons["add"]["title"]." ".$this->entity;
        $this->share_parameters();

        return View("backend.empresas.add");
    }

    public function store(Request $request)
    {
        $response_estructure = new ResponseEstructure();
        $response_estructure->set_response(false);

        $ambiente = trim($request->input("ambiente"));

        $razon_social = trim($request->input("razon_social"));
        $cuit = trim($request->input("cuit"));
        $domicilio_comercial = trim($request->input("domicilio_comercial"));
        $ingresos_brutos = trim($request->input("ingresos_brutos"));
        $inicio_actividades = trim($request->input("inicio_actividades"));
        $id_condicion_iva = trim($request->input("id_condicion_iva"));
        $logo_comprobante = $request->input("logo_comprobante");
        
        $input= [
            "ambiente" => $ambiente,

            "razon_social" => $razon_social,
            "cuit" => $cuit,
            "domicilio_comercial" => $domicilio_comercial,
            "ingresos_brutos" => $ingresos_brutos,
            "inicio_actividades" => $inicio_actividades,
            "id_condicion_iva" => $id_condicion_iva
        ];

        $rules = [
            "ambiente"=>"required",

            "razon_social" =>"required",
            "cuit" =>"required",
            "domicilio_comercial" =>"required",
            "ingresos_brutos" =>"required",
            "inicio_actividades" =>"required",
            "id_condicion_iva" =>"required",
        ];

        $validator = Validator::make($input, $rules);

        if ($validator->fails()) {
            $response_estructure->set_response(false);

            $errors = $validator->errors();

            foreach ($errors->all() as $error) {
                $response_estructure->add_message_error($error);
            }
        }
        else
        {
            $row_obj = new ConfiguracionAfip();

            $row_obj->ambiente = $ambiente;
            $row_obj->razon_social = $razon_social;
            $row_obj->cuit = $cuit;
            $row_obj->domicilio_comercial = $domicilio_comercial;
            $row_obj->ingresos_brutos = $ingresos_brutos;
            $row_obj->inicio_actividades = $inicio_actividades;
            $row_obj->id_condicion_iva = $id_condicion_iva;
            $row_obj->logo_comprobante = $logo_comprobante;
            
            $row_obj->save();

            // GUARDANDO DATOS DE PRODUCCION
            if(env('GENERACION_AUTOMATICA_CSR_AFIP') == 1)
            {
                $ds = DIRECTORY_SEPARATOR;
                $DIRECTORY_TO_SAVE = storage_path('app').$ds."certificados_ws/".$ambiente."/";

                if(file_exists($DIRECTORY_TO_SAVE.'request_csr_afip_'.$row_obj->id.'.csr'))
                {
                    unlink($DIRECTORY_TO_SAVE.'request_csr_afip_'.$row_obj->id.'.csr');
                }

                if(file_exists($DIRECTORY_TO_SAVE.'private_server_'.$row_obj->id.'.key'))
                {
                    unlink($DIRECTORY_TO_SAVE.'private_server_'.$row_obj->id.'.key');
                }

                $row_obj->csr_afip_produccion = $this->generarCsrParaAfip("produccion",$razon_social,$cuit,$row_obj);
                $row_obj->csr_afip_testing = $this->generarCsrParaAfip("testing",$razon_social,$cuit,$row_obj);

                $row_obj->save();
            }
            
            $response_estructure->set_response(true);
            $response_estructure->set_data(["id_agregado"=>$row_obj->id]);
        }
        
        return response()->json($response_estructure->get_response_array());
    }

    public function editar(Request $request,$id)
    {
        $this->title_page = $this->config_buttons["edit"]["title"]." ".$this->entity;
        $this->share_parameters();

        $row_obj = ConfiguracionAfip::where("id",$id)->first();

        if($row_obj)
        {
            return View("backend.empresas.edit")
            ->with("row_obj",$row_obj);
        }
    }

    public function update(Request $request)
    {
        $response_estructure = new ResponseEstructure();
        $response_estructure->set_response(false);

        $id = $request->input("id");
        $row_obj = ConfiguracionAfip::find($id);
        
        if($row_obj)
        {
            $ambiente = trim($request->input("ambiente"));

            $razon_social = trim($request->input("razon_social"));
            $cuit = trim($request->input("cuit"));
            $domicilio_comercial = trim($request->input("domicilio_comercial"));
            $ingresos_brutos = trim($request->input("ingresos_brutos"));
            $inicio_actividades = trim($request->input("inicio_actividades"));
            $id_condicion_iva = trim($request->input("id_condicion_iva"));
            $logo_comprobante = $request->input("logo_comprobante");

            $key_afip_produccion = trim($request->input("key_afip_produccion"));
            $key_afip_testing = trim($request->input("key_afip_testing"));
            
            $input= [
                "ambiente" => $ambiente,

                "razon_social" => $razon_social,
                "cuit" => $cuit,
                "domicilio_comercial" => $domicilio_comercial,
                "ingresos_brutos" => $ingresos_brutos,
                "inicio_actividades" => $inicio_actividades,
                "id_condicion_iva" => $id_condicion_iva,

                "key_afip_produccion"=>$key_afip_produccion,
                "key_afip_testing"=>$key_afip_testing,
            ];

            $rules = [
                "ambiente"=>"required",

                "razon_social" =>"required",
                "cuit" =>"required",
                "domicilio_comercial" =>"required",
                "ingresos_brutos" =>"required",
                "inicio_actividades" =>"required",
                "id_condicion_iva" =>"required",
            ];

            $validator = Validator::make($input, $rules);

            if ($validator->fails()) {
                $response_estructure->set_response(false);

                $errors = $validator->errors();

                foreach ($errors->all() as $error) {
                    $response_estructure->add_message_error($error);
                }
            }
            else
            {
                // GUARDANDO DATOS DE PRODUCCION
                if($row_obj->razon_social != $razon_social)
                {
                    if(env('GENERACION_AUTOMATICA_CSR_AFIP') == 1)
                    {
                        $row_obj->csr_afip_produccion = $this->generarCsrParaAfip("produccion",$razon_social,$cuit,$row_obj);
                        $row_obj->csr_afip_testing = $this->generarCsrParaAfip("testing",$razon_social,$cuit,$row_obj);
                    }
                }
                else
                {
                    if(trim($row_obj->csr_afip_produccion) == "")
                    {
                        $row_obj->csr_afip_produccion = $this->generarCsrParaAfip("produccion",$razon_social,$cuit,$row_obj);
                    }

                    if(trim($row_obj->csr_afip_testing) == "")
                    {
                        $row_obj->csr_afip_testing = $this->generarCsrParaAfip("testing",$razon_social,$cuit,$row_obj);
                    }
                }

                $row_obj->ambiente = $ambiente;
                $row_obj->razon_social = $razon_social;
                $row_obj->cuit = $cuit;
                $row_obj->domicilio_comercial = $domicilio_comercial;
                $row_obj->ingresos_brutos = $ingresos_brutos;
                $row_obj->inicio_actividades = $inicio_actividades;
                $row_obj->id_condicion_iva = $id_condicion_iva;
                $row_obj->logo_comprobante = $logo_comprobante;

                $row_obj->key_afip_produccion = $key_afip_produccion;
                $this->guardarKeyDeAfip("produccion",$key_afip_produccion);

                $row_obj->key_afip_testing = $key_afip_testing;
                $this->guardarKeyDeAfip("testing",$key_afip_testing);
                
                $row_obj->save();
                $response_estructure->set_response(true);
            }
        }
        else
        {
            $response_estructure->set_response(false);
            $response_estructure->add_message_error("Empresa no encontrada");
        }
        
        return response()->json($response_estructure->get_response_array());
    }

    private function generarCsrParaAfip($ambiente,$razon_social,$cuit,$row_obj)
    {
        $id_configuracion_afip = $row_obj->id;

        $ds = DIRECTORY_SEPARATOR;

        $DIRECTORY_TO_SAVE = storage_path('app').$ds."certificados_ws/".$ambiente."/";

        exec("openssl genrsa -out ".$DIRECTORY_TO_SAVE."private_server_".$id_configuracion_afip.".key 2048");
        exec("openssl req -new -key ".$DIRECTORY_TO_SAVE."private_server_".$id_configuracion_afip.".key -subj '/C=AR/O=".$razon_social."/CN=".env('CN_AFIP')."/serialNumber=CUIT ".$cuit."' -out ".$DIRECTORY_TO_SAVE."request_csr_afip_".$id_configuracion_afip.".csr");

        if(!file_exists($DIRECTORY_TO_SAVE.'request_csr_afip_'.$id_configuracion_afip.'.csr'))
        {
            file_put_contents($DIRECTORY_TO_SAVE.'request_csr_afip_'.$id_configuracion_afip.'.csr','');
        }

        return file_get_contents($DIRECTORY_TO_SAVE.'request_csr_afip_'.$id_configuracion_afip.'.csr');
    }

    private function guardarKeyDeAfip($ambiente,$key)
    {
        $id_configuracion_afip = session("id_configuracion_afip");

        $ds = DIRECTORY_SEPARATOR;
        $DIRECTORY_TO_SAVE = storage_path('app').$ds."certificados_ws/".$ambiente."/";
        file_put_contents($DIRECTORY_TO_SAVE.'key_from_afip_'.$id_configuracion_afip.'.key',$key);
    }

    public function descargarCsrParaAfip(Request $request)
    {
        $id_configuracion_afip = $request->input("id");
        $fileName = "request_csr_afip_".$id_configuracion_afip.".csr";
        $ambiente = $request->input("ambiente");
        
        $row_obj = ConfiguracionAfip::find($id_configuracion_afip);

        if($row_obj && ($ambiente == "produccion" || $ambiente == "testing"))
        {
            $ds = DIRECTORY_SEPARATOR;
            $DIRECTORY_TO_READ = storage_path('app').$ds."certificados_ws/".$ambiente."/";

            if(!file_exists($DIRECTORY_TO_READ.'request_csr_afip_'.$id_configuracion_afip.'.csr'))
            {
                file_put_contents($DIRECTORY_TO_READ.'request_csr_afip_'.$id_configuracion_afip.'.csr','');
            }

            $content = file_get_contents($DIRECTORY_TO_READ.$fileName);

            $headers = [
                'Content-type' => 'text/plain', 
                'Content-Disposition' => sprintf('attachment; filename="%s"', $fileName),
                'Content-Length' => strlen($content)
            ];

            return Response::make($content, 200, $headers);
        }
    }

    public function delete(Request $request)
    {
        $response_estructure = new ResponseEstructure();
        $response_estructure->set_response(false);

        $id = $request->input("id");

        $row_obj = ConfiguracionAfip::where("id",$id)->first();
        
        if($row_obj)
        {
            $row_obj->eliminado = 1;
            $row_obj->save();

            $response_estructure->set_response(true);
        }
        else
        {
            $response_estructure->set_response(false);
            $response_estructure->add_message_error($this->text_no_search);
        }


        return response()->json($response_estructure->get_response_array());
    }
}
