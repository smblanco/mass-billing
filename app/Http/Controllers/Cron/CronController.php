<?php

namespace App\Http\Controllers\Cron;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Remate;

class CronController extends Controller
{
    public function aviso_ganadores_subastas()
    {
        $remates_rows = Remate::where("fecha_hasta","<",Date("Y-m-d H:i:s"))
        ->where("remate_cerrado",0)->get();

        foreach($remates_rows as $remate_obj)
        {
            $remate_obj->remate_cerrado = true;
            $remate_obj->save();

            $remate_obj->enviar_correos_ganadores();
        }
    }
}
