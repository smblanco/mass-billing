<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use App\Library\ResponseEstructure;
use Illuminate\Support\Facades\Validator;

use App\Plan;
use App\ComentarioCliente;

class HomeController extends Controller
{
    public function index(Request $request)
    {
      $planes = Plan::where("eliminado",0)->where("activo",1)->orderBy("precio")->get();
      $comentarios_clientes = ComentarioCliente::where("activo",1)->get();

      return View("frontend.home")
      ->with("planes",$planes)
      ->with("comentarios_clientes",$comentarios_clientes);
    }
    
    /*
    public function suscribirme(Request $request)
    {
      $response_estructure = new ResponseEstructure();
      $response_estructure->set_response(false);

      $nombre = ucwords(trim($request->input("nombre")));
      $correo = trim($request->input("correo"));
      $telefono = trim($request->input("telefono"));
      $mensaje = trim($request->input("mensaje"));

      $input = [
        "nombre" => $nombre,
        "correo" => $correo,
      ];

      $rules = [
        "nombre" => "required|min:3",
        "correo" => "required|email",
      ];

      $validator = Validator::make($input, $rules);

      if ($validator->fails()) {
          $response_estructure->set_response(false);

          $errors = $validator->errors();

          foreach ($errors->all() as $error) {
              $response_estructure->add_message_error($error);
          }
      }
      else
      {
        $new_newsletter = Newsletter::where("correo",$correo)->first();

        if(!$new_newsletter)
        {
          $new_newsletter = new Newsletter();
          $new_newsletter->nombre = $nombre;
          $new_newsletter->correo = $correo;
          $new_newsletter->save();
        }

        $response_estructure->set_response(true);
      }

      return response()->json($response_estructure->get_response_array());
    }*/
}
