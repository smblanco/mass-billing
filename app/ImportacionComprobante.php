<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

use App\EstadoComprobante;

class ImportacionComprobante extends Model
{
    public $table = "importacion_comprobantes";
}
