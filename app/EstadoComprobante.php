<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EstadoComprobante extends Model
{
    public $table = "estado_comprobante";

    const PENDIENTE = 1;
    const REALIZADO = 2;
    const ERROR_DE_VALIDACION = 3;
    const ERROR_DE_AFIP = 4;
}
