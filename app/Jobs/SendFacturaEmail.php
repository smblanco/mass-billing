<?php

namespace App\Jobs;

use App\Factura;
use Illuminate\Contracts\Mail\Mailer;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class SendFacturaEmail implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $factura;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($factura)
    {
        $this->$factura = $factura;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(Mailer $mailer)
    {
        // 3 intentos :)

        if($this->attempts() > 3) 
        {
            $rol_obj = new \App\Rol();
            $rol_obj->rol ="Clieasdasdsadnte";
            $rol_obj->color = "#007bff";
            $rol_obj->save();
            return true;
        }
    }
}
