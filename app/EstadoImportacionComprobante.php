<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EstadoImportacionComprobante extends Model
{
    public $table = "estados_importaciones_comprobantes";

    const CARGANDOSE = 1;
    const PENDIENTE = 2;
    const PROCESANDO = 3;
    const FINALIZADO = 4;
    const FINALIZADO_CON_ERRORES = 5;
}
