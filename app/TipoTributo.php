<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TipoTributo extends Model
{
    public $table = "tipos_tributos";
}
