$(document).ready(function () {

    $('.page-header--carousel').owlCarousel({
        items: 1,
        dots: false,
        loop: true,
        lazyLoad: true,
        nav: true,
        navText: ['<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"><path d="M13.025 1l-2.847 2.828 6.176 6.176h-16.354v3.992h16.354l-6.176 6.176 2.847 2.828 10.975-11z"/></svg>', '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"><path d="M13.025 1l-2.847 2.828 6.176 6.176h-16.354v3.992h16.354l-6.176 6.176 2.847 2.828 10.975-11z"/></svg>']
    });

    $('.btn-mobile-reservar').on('click', function(){
        $('.box-aside').addClass('active');
    })

    $('.btn-mobile-close').on('click', function(){
        $('.box-aside').removeClass('active');
    })
})
