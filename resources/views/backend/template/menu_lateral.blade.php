<!-- Sidebar -->
<div class="sidebar sidebar-style-2">
	<div class="sidebar-wrapper scrollbar scrollbar-inner">

		<div class="sidebar-content">
			<!--
			<div class="user">
				<div class="avatar-sm float-left mr-2">
					<img src="{{ url('storage/imagenes/usuarios/'.session('foto_perfil'))}}" alt="..." class="avatar-img rounded-circle">
				</div>
				<div class="info">
					<a data-toggle="collapse" href="#collapseExample" aria-expanded="true">
						<span>
							{{session("nombre")}}
							@if(session("id_rol") == 1)
							<span class="user-level">Administrator</span>
							@endif
							<span class="caret"></span>
						</span>
					</a>
					<div class="clearfix"></div>

					<div class="collapse in" id="collapseExample">
						<ul class="nav">
							<li>
								<a href="{{url('/backend/perfil')}}">
									<span class="link-collapse">Editar Perfil</span>
								</a>
							</li>
						</ul>
					</div>
				</div>
			</div>
			-->
			<ul class="nav nav-primary">

				<li class="nav-item active">
					<a href="{{url('backend/desktop')}}">
						<i class="fas fa-desktop"></i>
						<p>Escritorio</p>
					</a>
				</li>

				@if(session("id_rol") == 1)

				<li class="nav-item">
					<a data-toggle="collapse" href="#sidebarUsuarios">
						<i class="fas fa-users"></i>
						<p>Usuarios</p>
						<span class="caret"></span>
					</a>
					<div class="collapse" id="sidebarUsuarios">
						<ul class="nav nav-collapse">
							<li>
								<a href="{{url('/backend/usuarios')}}">
									<span class="sub-item">Listado</span>
								</a>
							</li>
							<li>
								<a href="{{url('/backend/usuarios/nuevo')}}">
									<span class="sub-item">Nuevo</span>
								</a>
							</li>
						</ul>
					</div>
				</li>

				<li class="nav-item">
					<a href="{{url('backend/ventanilla_electronica')}}">
						<i class="fas fa-bell"></i>
						<p>Ventanilla Electronica</p>
					</a>
				</li>

				<li class="nav-item">
					<a href="{{url('/backend/mis_clientes')}}">
						<i class="fas fa-users"></i>
						<p>Mis Clientes</p>
					</a>
				</li>

				<li class="nav-item">
					<a  href="{{url('/backend/productos_servicios')}}">
						<i class="fas fa-cube"></i>
						<p>Productos / Servicios</p>
					</a>
				</li>

				<li class="nav-item">
					<a data-toggle="collapse" href="#sidebarComprobantes">
						<i class="fas fa-list"></i>
						<p>Comprobantes</p>
						<span class="caret"></span>
					</a>
					<div class="collapse" id="sidebarComprobantes">
						<ul class="nav nav-collapse">
							<li>
								<a href="{{url('/backend/comprobantes')}}">
									<span class="sub-item">Mis Comprobantes</span>
								</a>
							</li>
							<li>
								<a href="{{url('/backend/comprobantes/nuevo')}}">
									<span class="sub-item">Nuevo</span>
								</a>
							</li>
							<li>
								<a href="{{url('/backend/comprobantes/importaciones')}}">
									<span class="sub-item">Importaciones</span>
								</a>
							</li>
							<li>
								<a href="{{url('/backend/comprobantes/mis_puntos_de_venta')}}">
									<span class="sub-item">Mis Puntos de venta</span>
								</a>
							</li>
						</ul>
					</div>
				</li>

				<li class="nav-item">
					<a data-toggle="collapse" href="#sidebarConfiguraciones">
						<i class="fas fa-cogs"></i>
						<p>Configuraciones</p>
						<span class="caret"></span>
					</a>
					<div class="collapse" id="sidebarConfiguraciones">
						<ul class="nav nav-collapse">
							<li>
								<a href="{{url('/backend/configuraciones/afip')}}">
									<span class="sub-item">AFIP</span>
								</a>
							</li>
							<li>
								<a href="{{url('/backend/configuraciones/smtp')}}">
									<span class="sub-item">SMTP</span>
								</a>
							</li>
						</ul>
					</div>
				</li>

				@endif

			</ul>
		</div>
	</div>
</div>
<!-- End Sidebar -->
