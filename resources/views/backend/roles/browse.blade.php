@extends('backend.template.master')

@section('title', 'Roles')

@section("breadcrumb")
<div class="row mb-2">
    <div class="col-sm-6">
    <h1>Roles</h1>
    </div>
    <div class="col-sm-6">
    <ol class="breadcrumb float-sm-right">
        <li class="breadcrumb-item"><a href="#">Roles</a></li>
        <li class="breadcrumb-item active">Listado</li>
    </ol>
    </div>
</div>
@endsection

@section('contenido')
<div class="row">
    <div class="col-12">
        <button onclick="abrir_modal_agregar()" class="{{$config_buttons['add']['class']}}">
            <i class="{{$config_buttons['add']['icon']}}"></i> {{ $config_buttons['add']['title'] }}
        </button>
    </div>
    <div class="col-12" style="margin-top: 10px;">
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col-md-12">
                        <table id="listado" class="table table-bordered table-striped" style="width: 100%;">
                            <thead>
                                <tr>
                                    <th>Rol</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                                
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /.col -->
</div>
<!-- /.row -->
@endsection


@section("modals")

    @component('backend.template.modal_agregar')
        @slot('entidad')
            Rol
        @endslot

        @slot('inputs')
            <div class="col-md-12">
                <label for="rol_agregar">Rol</label>
                <input type="text" class="form-control" id="rol_agregar" name="rol">
            </div>
        @endslot
    @endcomponent

    @component('backend.template.modal_editar')
        @slot('entidad')
            Rol
        @endslot

        @slot('inputs')
            <div class="col-md-12">
                <label for="rol_editar">Rol</label>
                <input type="text" class="form-control" id="rol_editar" name="rol">
            </div>
        @endslot
    @endcomponent

@endsection

@section("js_code")

<script type="text/javascript">
var listado = null;
var id_trabajando = 0;

$(document).ready( function () {
    listado= $('#listado').DataTable( {
          "processing": true,
          "serverSide": true,
          "responsive":false,
          "ajax":{
            url : "{{$link_controlador}}get_listado_dt", // json datasource
            type: "post",
            headers:
            {
              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            error: function(error){
              $(".employee-grid-error").html("");
              $("#employee-grid").append('<tbody class="employee-grid-error"><tr><th colspan="3">No hay datos</th></tr></tbody>');
              $("#employee-grid_processing").css("display","none");
            }
          }
      });
});

function abrir_modal_agregar()
{
    $("#rol_agregar").val("");
    $("#modal_agregar").modal("show");
}

function abrir_modal_editar(id)
{
    $.ajax({
        url: "{{$link_controlador}}get",
        type: "POST",
        data: {id:id},
        headers:
        {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        beforeSend: function(event){
          abrir_loading();
        },
        success: function(data)
        {
            cerrar_loading();

            try
            {
              if(data["response"] == true)
              {
                id_trabajando = id;
                $("#rol_editar").val(data["data"]["rol"]);
                $("#modal_editar").modal("show");
              }
              else
              {
                mostrar_mensajes_errores(data["messages_errors"]);
              }
            }
            catch(e)
            {
              mostrar_mensajes_errores();
            }

        },
        error: function(error){
          cerrar_loading();
          mostrar_mensajes_errores();
        },
    });
}

function agregar()
{
    var formdata = new FormData();

    formdata.append("rol",$("#rol_agregar").val());

    $.ajax({
        url: "{{$link_controlador}}storage",
        type: "POST",
        contentType: false,
        cache: false,
        processData:false,
        data: formdata,
        headers:
        {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        beforeSend: function(event){
          abrir_loading();
        },
        success: function(data)
        {
            cerrar_loading();

            try
            {
              if(data["response"] == true)
              {
                $("#modal_agregar").modal("hide");

                mostrar_mensajes_success(
                    "{{$abm_messages['success_add']['title']}}",
                    "{{$abm_messages['success_add']['description']}}"
                );

                listado.draw();
              }
              else
              {
                mostrar_mensajes_errores(data["messages_errors"]);
              }
            }
            catch(e)
            {
              mostrar_mensajes_errores();
            }

        },
        error: function(error){
          cerrar_loading();
          mostrar_mensajes_errores();
        },
    });
}

function editar()
{
    var formdata = new FormData();

    formdata.append("id",id_trabajando);
    formdata.append("rol",$("#rol_editar").val());

    $.ajax({
        url: "{{$link_controlador}}update",
        type: "POST",
        contentType: false,
        cache: false,
        processData:false,
        data: formdata,
        headers:
        {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        beforeSend: function(event){
          abrir_loading();
        },
        success: function(data)
        {
            cerrar_loading();

            try
            {
              if(data["response"] == true)
              {
                $("#modal_editar").modal("hide");

                mostrar_mensajes_success(
                    "{{$abm_messages['success_edit']['title']}}",
                    "{{$abm_messages['success_edit']['description']}}"
                );

                listado.draw();
              }
              else
              {
                mostrar_mensajes_errores(data["messages_errors"]);
              }
            }
            catch(e)
            {
              mostrar_mensajes_errores();
            }

        },
        error: function(error){
          cerrar_loading();
          mostrar_mensajes_errores();
        },
    });
}

function eliminar(id)
{
    swal({
        title: "{{$abm_messages['delete']['title']}}",
        text: "{{$abm_messages['delete']['text']}}",
        type: 'error',
        showCancelButton: true,
        confirmButtonText: "{{$abm_messages['delete']['confirmButtonText']}}",
        cancelButtonText: "{{$abm_messages['delete']['cancelButtonText']}}",
        confirmButtonClass: 'btn btn-success',
        cancelButtonClass: 'btn btn-danger m-l-10',
        buttonsStyling: false
    }).then(function () {

        $.ajax({
            url: "{{$link_controlador}}delete",
            type: "POST",
            data: {id:id},
            headers:
            {
              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            beforeSend: function(event){
              abrir_loading();
            },
            success: function(data)
            {
                cerrar_loading();

                try{

                    if(data["response"])
                    {
                        mostrar_mensajes_success(
                            "{{$abm_messages['delete']['success_text']}}",
                            "{{$abm_messages['delete']['success_description']}}"
                        );

                        listado.draw();
                    }
                    else
                    {
                        mostrar_mensajes_errores(data["messages_errors"]);
                    }
                  }
                  catch(e)
                  {
                    mostrar_mensajes_errores();
                  }
            },
            error: function(error){
                cerrar_loading();
                mostrar_mensajes_errores();
            },
        });
    }, function (dismiss) {
        if (dismiss === 'cancel') {}
    });
}

</script>

@endsection