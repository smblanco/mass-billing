@extends('backend.template.master')

@section('title', $title_page)

@section('contenido')
<div class="main-panel">
    <div class="content">
        <div class="page-inner">
            <div class="page-header">
                <h4 class="page-title">{{$title_page}}</h4>
            </div>

            <div class="row">

                <div class="col-md-12">
                    <div class="card">
                        <div class="card-body">
                            <h3 class="text-center">La creación de comprobantes solo se encuentra disponible para monotributistas</h3>
                        </div>
                    </div>
                </div>
			</div>
		</div>
    </div>
</div>
@endsection


@section("modals")
@endsection

@section("js_code")

<script type="text/javascript">
</script>

@endsection
