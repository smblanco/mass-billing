<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Conceptos extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create("conceptos",function(Blueprint $table){
            $table->integer('Id');
            $table->string("Desc",150);
            $table->integer("FchDesde")->nullable();
            $table->integer("FchHasta")->nullable();
            $table->primary('Id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0');
        Schema::dropIfExists('conceptos');
        DB::statement('SET FOREIGN_KEY_CHECKS=1');
    }
}
