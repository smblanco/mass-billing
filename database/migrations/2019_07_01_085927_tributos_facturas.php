<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TributosFacturas extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create("tributos_comprobantes",function(Blueprint $table){
            $table->increments("id")->unsigned();

            $table->string("descripcion");
            $table->string("detalle");
            $table->double("base_imponible");
            $table->double("alicuota");
            $table->double("importe");
            $table->integer("id_tipo_tributo")->unsigned();
            $table->foreign("id_tipo_tributo")->references("id")->on("tipos_tributos");
            $table->integer("id_comprobante")->unsigned();
            $table->foreign("id_comprobante")->references("id")->on("comprobantes");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0');
        Schema::dropIfExists('tributos_comprobantes');
        DB::statement('SET FOREIGN_KEY_CHECKS=1');
    }
}
