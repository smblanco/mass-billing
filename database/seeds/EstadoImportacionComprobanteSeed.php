<?php

use Illuminate\Database\Seeder;
use App\EstadoImportacionComprobante;

class EstadoImportacionComprobanteSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        EstadoImportacionComprobante::truncate();

        $estado_importacion_comprobante = new EstadoImportacionComprobante();
        $estado_importacion_comprobante->estado = "CARGANDOSE";
        $estado_importacion_comprobante->color ="#6c757d";
        $estado_importacion_comprobante->save();
        
        $estado_importacion_comprobante = new EstadoImportacionComprobante();
        $estado_importacion_comprobante->estado = "PENDIENTE";
        $estado_importacion_comprobante->color ="#d39e00";
        $estado_importacion_comprobante->save();
        
        $estado_importacion_comprobante = new EstadoImportacionComprobante();
        $estado_importacion_comprobante->estado = "PROCESANDO";
        $estado_importacion_comprobante->color ="#0062cc";
        $estado_importacion_comprobante->save();

        $estado_importacion_comprobante = new EstadoImportacionComprobante();
        $estado_importacion_comprobante->estado = "FINALIZADO";
        $estado_importacion_comprobante->color ="#1e7e34";
        $estado_importacion_comprobante->save();

        $estado_importacion_comprobante = new EstadoImportacionComprobante();
        $estado_importacion_comprobante->estado = "FINALIZADO CON ERRORES";
        $estado_importacion_comprobante->color ="#bd2130";
        $estado_importacion_comprobante->save();
    }
}
