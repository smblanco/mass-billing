<?php

use Illuminate\Database\Seeder;
use App\EstadoComprobante;

class EstadoComprobanteSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        EstadoComprobante::truncate();

        $estado_comprobante = new EstadoComprobante();        
        $estado_comprobante->estado = "PENDIENTE";
        $estado_comprobante->color = "#ffc107";
        $estado_comprobante->save();

        $estado_comprobante = new EstadoComprobante();        
        $estado_comprobante->estado = "REALIZADO";
        $estado_comprobante->color = "#28a745";
        $estado_comprobante->save();

        $estado_comprobante = new EstadoComprobante();        
        $estado_comprobante->estado = "ERROR_DE_VALIDACION";
        $estado_comprobante->color = "#dc3545";
        $estado_comprobante->save();

        $estado_comprobante = new EstadoComprobante();        
        $estado_comprobante->estado = "ERROR_DE_AFIP";
        $estado_comprobante->color = "#dc3545";
        $estado_comprobante->save();
    }
}
