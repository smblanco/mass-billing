<?php

use Illuminate\Database\Seeder;
use App\CondicionDeVenta;

class CondicionDeVentaSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        CondicionDeVenta::truncate();

        $condicion_de_venta_obj = new CondicionDeVenta();
        $condicion_de_venta_obj->id = 1;
        $condicion_de_venta_obj->descripcion = "Contado";
        $condicion_de_venta_obj->save();

        $condicion_de_venta_obj = new CondicionDeVenta();
        $condicion_de_venta_obj->id = 69;
        $condicion_de_venta_obj->descripcion = "Tarjeta de Débito";
        $condicion_de_venta_obj->save();
        
        $condicion_de_venta_obj = new CondicionDeVenta();
        $condicion_de_venta_obj->id = 68;
        $condicion_de_venta_obj->descripcion = "Tarjeta de Crédito";
        $condicion_de_venta_obj->save();
        
        $condicion_de_venta_obj = new CondicionDeVenta();
        $condicion_de_venta_obj->id = 96;
        $condicion_de_venta_obj->descripcion = "Cuenta Corriente";
        $condicion_de_venta_obj->save();
        
        $condicion_de_venta_obj = new CondicionDeVenta();
        $condicion_de_venta_obj->id = 97;
        $condicion_de_venta_obj->descripcion = "Cheque";
        $condicion_de_venta_obj->save();
        
        $condicion_de_venta_obj = new CondicionDeVenta();
        $condicion_de_venta_obj->id = 98;
        $condicion_de_venta_obj->descripcion = "Boleto";
        $condicion_de_venta_obj->save();
        
        $condicion_de_venta_obj = new CondicionDeVenta();
        $condicion_de_venta_obj->id = 99;
        $condicion_de_venta_obj->descripcion = "Otra";
        $condicion_de_venta_obj->save();
    }
}
