<?php

use Illuminate\Database\Seeder;
use App\Importacion;

class ImportacionSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Importacion::truncate();
    }
}
